<?php
require( 'model/Model.php' );
require( "model/UserModel.php" );

class User
{
	const SALT = 'qwe123';

	private static $authorised = false;
	private static $user = null;

	public static function login($login, $pass)
	{
		$pass_hash = md5( self::SALT . $pass );
		$mysqli_result = UserModel::get_user_by_login( $login, $pass_hash );
		$user = $mysqli_result->fetch_object();

		if ( $user ) {
			self::$authorised = true;
		}

		self::$user = $user;
	}

	public static function register($login, $pass, $first_name, $last_name, $patronymic, $email)
	{
		$pass_hash = md5( self::SALT . $pass );
		UserModel::create_user( $login, $pass_hash, $first_name, $last_name, $patronymic, $email );
	}

	public static function update($login, $first_name, $last_name, $patronymic, $pass)
	{
		if ( $pass === '' ) {
			$pass = $_SESSION['pass'];
		}
		$pass_hash = md5( self::SALT . $pass );
		UserModel::update_user_by_login( $login, $first_name, $last_name, $patronymic, $pass_hash );
	}

	public static function is_authorised()
	{
		return self::$authorised;
	}

	public static function get_current_user()
	{
		return self::$user;
	}
}
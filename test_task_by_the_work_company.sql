-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Авг 05 2020 г., 14:20
-- Версия сервера: 10.3.13-MariaDB-log
-- Версия PHP: 7.1.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `test_task_by_the_work_company`
--

-- --------------------------------------------------------

--
-- Структура таблицы `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `price`) VALUES
(1, 1, 1000),
(4, 6, 1000),
(5, 7, 1000),
(6, 1, 1000),
(7, 1, 1000),
(8, 7, 1000);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `login` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `patronymic` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `passwd` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `email`, `login`, `first_name`, `last_name`, `patronymic`, `passwd`) VALUES
(1, 'a@a.a', 'user', 'F', 'L', 'P', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(3, 'a495@a.a', 'user495', 'First-495', 'Last', 'Patron', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(6, 'msnufflebottom0@goodreads.com', 'user1', 'Maia', 'Snufflebottom', 'Greek', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(7, 'bklemencic1@nifty.com', 'user2', 'Bari', 'Klemencic', 'Spanish', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(8, 'gscawen2@artisteer.com', 'user3', 'Gabrielle', 'Scawen', 'Korean', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(9, 'mivchenko3@forbes.com', 'user4', 'Marinna', 'Ivchenko', 'Hebrew', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(10, 'acoda4@reference.com', 'user5', 'Aretha', 'Coda', 'Tetum', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(11, 'efullom5@symantec.com', 'user6', 'Errick', 'Fullom', 'Spanish', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(12, 'rthirwell6@phpbb.com', 'user7', 'Rori', 'Thirwell', 'Somali', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(13, 'kminchi7@furl.net', 'user8', 'Kippie', 'Minchi', 'Pashto', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(14, 'zbanghe8@cafepress.com', 'user9', 'Zorina', 'Banghe', 'Swati', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(15, 'dmcilhagga9@flavors.me', 'user10', 'Doralynn', 'McIlhagga', 'Macedonian', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(16, 'bgirdlera@dot.gov', 'user11', 'Buddy', 'Girdler', 'Tamil', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(17, 'qlanslyb@feedburner.com', 'user12', 'Quinlan', 'Lansly', 'Hebrew', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(18, 'rthirwell6@phpbb.com', 'user13', 'Sharla', 'McCalum', 'Somali', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(19, 'ozarfatid@ucla.edu', 'user14', 'Ogdon', 'Zarfati', 'Bulgarian', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(20, 'rbarthrupe@fda.gov', 'user15', 'Rodger', 'Barthrup', 'Somali', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(21, 'bfeasleyf@vimeo.com', 'user16', 'Blayne', 'Feasley', 'Assamese', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(22, 'kgherardescig@wunderground.com', 'user17', 'Kirby', 'Gherardesci', 'Bengali', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(23, 'ggoodingsh@google.co.jp', 'user18', 'Graehme', 'Goodings', 'Malayalam', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(24, 'gmcgluei@soundcloud.com', 'user19', 'Ginelle', 'McGlue', 'Punjabi', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(25, 'rthirwell6@phpbb.com', 'user20', 'Roselin', 'McTerrelly', 'Dhivehi', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(26, 'gcoylek@ca.gov', 'user21', 'Garrett', 'Coyle', 'Latvian', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(27, 'fschurickel@purevolume.com', 'user22', 'Flemming', 'Schuricke', 'Hindi', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(28, 'rthirwell6@phpbb.com', 'user23', 'Jamaal', 'Enstone', 'English', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(29, 'acollefordn@yolasite.com', 'user24', 'Arly', 'Colleford', 'Greek', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(30, 'bspellacyo@trellian.com', 'user25', 'Bent', 'Spellacy', 'Haitian Creole', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(31, 'csciacovellip@geocities.com', 'user26', 'Charmion', 'Sciacovelli', 'Kazakh', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(32, 'eoxerq@army.mil', 'user27', 'Edyth', 'Oxer', 'Northern Sotho', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(33, 'sguidoner@furl.net', 'user28', 'Somerset', 'Guidone', 'Nepali', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(34, 'ctrottons@slate.com', 'user29', 'Christy', 'Trotton', 'Dhivehi', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(35, 'jmapledorumt@nature.com', 'user30', 'Jeralee', 'Mapledorum', 'Czech', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(36, 'jlaveru@github.com', 'user31', 'Jan', 'Laver', 'Swedish', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(37, 'jdickiev@printfriendly.com', 'user32', 'Jolee', 'Dickie', 'Ndebele', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(38, 'lwheadonw@nba.com', 'user33', 'Lisbeth', 'Wheadon', 'Estonian', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(39, 'rbarthrupe@fda.gov', 'user34', 'Meredeth', 'Houseley', 'Somali', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(40, 'hrihaneky@home.pl', 'user35', 'Hildy', 'Rihanek', 'Belarusian', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(41, 'bbuxeyz@nih.gov', 'user36', 'Bennett', 'Buxey', 'French', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(42, 'gfigin10@ycombinator.com', 'user37', 'Gery', 'Figin', 'Malagasy', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(43, 'cboribal11@sakura.ne.jp', 'user38', 'Corny', 'Boribal', 'Danish', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(44, 'rbarthrupe@fda.gov', 'user39', 'Lindy', 'Sinisbury', 'Zulu', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(45, 'fsighard13@time.com', 'user40', 'Frankie', 'Sighard', 'Icelandic', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(46, 'mshellum14@hud.gov', 'user41', 'Mae', 'Shellum', 'Bosnian', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(47, 'lpaula15@boston.com', 'user42', 'Loralie', 'Paula', 'Persian', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(48, 'rbarthrupe@fda.gov', 'user43', 'Saw', 'Kloisner', 'Oriya', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(49, 'athornally17@miibeian.gov.cn', 'user44', 'Ardyth', 'Thornally', 'Irish Gaelic', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(50, 'hvanacci18@list-manage.com', 'user45', 'Hedda', 'Vanacci', 'Kyrgyz', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(51, 'mcallicott19@macromedia.com', 'user46', 'Maurise', 'Callicott', 'Telugu', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(52, 'baspinall1a@pinterest.com', 'user47', 'Barth', 'Aspinall', 'West Frisian', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(53, 'oletherbury1b@macromedia.com', 'user48', 'Ozzie', 'Letherbury', 'Thai', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(54, 'gkennon1c@cnn.com', 'user49', 'Gerardo', 'Kennon', 'Thai', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(55, 'ryeldon1d@discovery.com', 'user50', 'Rosemaria', 'Yeldon', 'Portuguese', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(56, 'btorrance1e@diigo.com', 'user51', 'Bryon', 'Torrance', 'Swati', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(57, 'jgiffaut1f@paginegialle.it', 'user52', 'Jessalyn', 'Giffaut', 'Malayalam', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(58, 'jlaveru@github.com', 'user53', 'Fleurette', 'Edlington', 'Georgian', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(59, 'mshilleto1h@jigsy.com', 'user54', 'Morna', 'Shilleto', 'Montenegrin', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(60, 'gstuttard1i@vinaora.com', 'user55', 'Garrot', 'Stuttard', 'Czech', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(61, 'dbartolomeo1j@prlog.org', 'user56', 'Darnall', 'Bartolomeo', 'Japanese', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(62, 'jlaveru@github.com', 'user57', 'Hanny', 'Tulloch', 'New Zealand Sign Language', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(63, 'cgladtbach1l@google.de', 'user58', 'Clay', 'Gladtbach', 'Yiddish', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(64, 'jpirrey1m@printfriendly.com', 'user59', 'Jackelyn', 'Pirrey', 'Aymara', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(65, 'jlaveru@github.com', 'user60', 'Ansel', 'Alvarado', 'Malayalam', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(66, 'acolquite1o@dedecms.com', 'user61', 'Aryn', 'Colquite', 'Telugu', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(67, 'ldominici1p@google.com.au', 'user62', 'Loise', 'Dominici', 'Sotho', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(68, 'croyston1q@flickr.com', 'user63', 'Con', 'Royston', 'German', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(69, 'jlaveru@github.com', 'user64', 'Julie', 'Thredder', 'Aymara', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(70, 'ketherson1s@drupal.org', 'user65', 'Karlan', 'Etherson', 'Persian', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(71, 'dblunkett1t@photobucket.com', 'user66', 'Dulcinea', 'Blunkett', 'Macedonian', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(72, 'hkelberman1u@cbsnews.com', 'user67', 'Hector', 'Kelberman', 'Kashmiri', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(73, 'acox1v@house.gov', 'user68', 'Arturo', 'Cox', 'Macedonian', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(74, 'nlowres1w@state.tx.us', 'user69', 'Naoma', 'Lowres', 'Dari', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(75, 'lheater1x@photobucket.com', 'user70', 'Liz', 'Heater', 'Tswana', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(76, 'gharner1y@usa.gov', 'user71', 'Grant', 'Harner', 'Tok Pisin', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(77, 'zklink1z@ted.com', 'user72', 'Zilvia', 'Klink', 'Greek', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(78, 'cfreshwater20@liveinternet.ru', 'user73', 'Clovis', 'Freshwater', 'Georgian', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(79, 'aroomes21@trellian.com', 'user74', 'Anatol', 'Roomes', 'Persian', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(80, 'jmounce22@wufoo.com', 'user75', 'Jody', 'Mounce', 'Azeri', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(81, 'bbarltrop23@alexa.com', 'user76', 'Brannon', 'Barltrop', 'Assamese', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(82, 'babrahmovici24@amazon.co.jp', 'user77', 'Bealle', 'Abrahmovici', 'Haitian Creole', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(83, 'cdomelaw25@flavors.me', 'user78', 'Cris', 'Domelaw', 'Filipino', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(84, 'afairbanks26@washingtonpost.com', 'user79', 'Arnaldo', 'Fairbanks', 'Armenian', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(85, 'cchitson27@weibo.com', 'user80', 'Curt', 'Chitson', 'Papiamento', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(86, 'tfripps28@cnn.com', 'user81', 'Teena', 'Fripps', 'Mongolian', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(87, 'tkaes29@yolasite.com', 'user82', 'Tam', 'Kaes', 'Luxembourgish', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(88, 'acleugh2a@si.edu', 'user83', 'Arney', 'Cleugh', 'Tajik', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(89, 'woshee2b@springer.com', 'user84', 'Wendel', 'O\'Shee', 'Nepali', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(90, 'qjarret2c@washingtonpost.com', 'user85', 'Quintana', 'Jarret', 'Tsonga', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(91, 'aphelp2d@nhs.uk', 'user86', 'Ailis', 'Phelp', 'Persian', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(92, 'slejeune2e@51.la', 'user87', 'Starlin', 'Lejeune', 'Fijian', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(93, 'sbolletti2f@rakuten.co.jp', 'user88', 'Stacy', 'Bolletti', 'Pashto', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(94, 'skezar2g@pagesperso-orange.fr', 'user89', 'Sibelle', 'Kezar', 'Luxembourgish', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(95, 'wadlam2h@liveinternet.ru', 'user90', 'Welsh', 'Adlam', 'Filipino', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(96, 'zscripps2i@reuters.com', 'user91', 'Zahara', 'Scripps', 'Swedish', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(97, 'aargente2j@google.ca', 'user92', 'Anna-maria', 'Argente', 'Bengali', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(98, 'ephelp2k@rediff.com', 'user93', 'Esther', 'Phelp', 'Kazakh', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(99, 'dmcdiarmid2l@harvard.edu', 'user94', 'Dunstan', 'McDiarmid', 'Quechua', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(100, 'mstraun2m@google.cn', 'user95', 'Marshal', 'Straun', 'Spanish', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(101, 'gfriett2n@toplist.cz', 'user96', 'Gavrielle', 'Friett', 'Kurdish', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(102, 'jmarioneau2o@mozilla.org', 'user97', 'Jenilee', 'Marioneau', 'Fijian', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(103, 'gmackowle2p@nsw.gov.au', 'user98', 'Gianna', 'MacKowle', 'Chinese', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(104, 'astreatfield2q@xrea.com', 'user99', 'Artur', 'Streatfield', 'Fijian', 'ee0b5fc393b05f2626bafa92e4f74d91'),
(105, 'hmcgifford2r@domainmarket.com', 'user100', 'Heindrick', 'McGifford', 'Punjabi', 'ee0b5fc393b05f2626bafa92e4f74d91');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `login` (`login`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=106;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php

session_start();

// already authorised
if ( isset( $_SESSION['login'] ) && isset( $_SESSION['pass'] ) ) {
	header( "HTTP/1.1 302 Moved Temporarily" );
	header( "Location: /" );
}

?>

<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta
		name="viewport"
		content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0"
	>
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Login</title>
</head>
<body style="text-align: center;">
<form action="/" method="post" style="display: inline-block; text-align: right;">
	<h2 style="text-align: center">Login</h2>
	<label>Login: <input type="text" name="login" value="user" required /></label><br>
	<label>Password: <input type="password" name="pass" value="1" required /></label><br>
	<input type="hidden" name="action" value="login" />
	<input type="submit" value="login" /><br><br>
	<a href="/register.php">Register new account</a>
</form>
</body>
</html>

<?php

session_start();

unset( $_SESSION['login'] );
unset( $_SESSION['pass'] );

header( "HTTP/1.1 302 Moved Temporarily" );
header( "Location: /login.php" );

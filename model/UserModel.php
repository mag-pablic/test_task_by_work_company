<?php

class UserModel extends Model
{
	public static function create_user($login, $pass, $first_name, $last_name, $patronymic, $email)
	{
		$login = self::escape_string( $login );
		$pass = self::escape_string( $pass );
		$first_name = self::escape_string( $first_name );
		$last_name = self::escape_string( $last_name );
		$patronymic = self::escape_string( $patronymic );
		$email = self::escape_string( $email );
		$query_string = "INSERT INTO users (login, email, passwd, first_name, last_name, patronymic) VALUES (
			'{$login}',
			'{$email}',
			'{$pass}',
			'{$first_name}',
			'{$last_name}',
			'{$patronymic}'
		);";
		return self::query( $query_string );
	}

	public static function get_user_by_login($login, $pass)
	{
		$login = self::escape_string( $login );
		$pass = self::escape_string( $pass );
		$query_string = "
			SELECT 
				login, 
				email, 
				first_name,
				last_name, 
				patronymic 
			FROM 
				`users` 
			WHERE 
				login = '{$login}' AND passwd = '{$pass}';
		";
		return self::query( $query_string );
	}

	public static function update_user_by_login($login, $first_name, $last_name, $patronymic, $pass)
	{
		$login = self::escape_string( $login );
		$pass = self::escape_string( $pass );
		$first_name = self::escape_string( $first_name );
		$last_name = self::escape_string( $last_name );
		$patronymic = self::escape_string( $patronymic );
		$query_string = "
			UPDATE 
				`users` 
			SET
				`first_name` = '{$first_name}',
				`last_name` = '{$last_name}',
				`patronymic` = '{$patronymic}',
				`passwd` = '{$pass}'
			WHERE
				`login` = '{$login}'
		";
		return self::query( $query_string );
	}
}
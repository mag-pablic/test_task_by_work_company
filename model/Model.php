<?php

class Model
{
	private static $mysqli;

	public static function connect_db()
	{
		self::$mysqli = new mysqli(
			"127.0.0.1",
			"root",
			"",
			"test_task_by_the_work_company",
			3306
		);

		if ( self::$mysqli->connect_errno ) {
			echo "MySQL connect error: (" . self::$mysqli->connect_errno . ") " . self::$mysqli->connect_error;
		}
	}

	public static function escape_string($param)
	{
		return self::$mysqli->real_escape_string( $param );
	}

	public static function query($query_string)
	{
		return self::$mysqli->query( $query_string );
	}
}

Model::connect_db();

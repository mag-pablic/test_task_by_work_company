<?php

session_start();

// already authorised
if ( isset( $_SESSION['login'] ) && isset( $_SESSION['pass'] ) ) {
	header( "HTTP/1.1 302 Moved Temporarily" );
	header( "Location: /" );
}

?>

<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta
		name="viewport"
		content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0"
	>
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Register</title>
</head>
<body style="text-align: center;">
<form action="/" method="post" style="display: inline-block; text-align: right;">
	<h2 style="text-align: center">Register</h2>
	<label>Login: <input type="text" name="login" value="" required /></label><br>
	<label>Email: <input type="text" name="email" value="" /></label><br>
	<label>First name: <input type="text" name="first-name" value="" required /></label><br>
	<label>Last name: <input type="text" name="last-name" value="" required /></label><br>
	<label>Patronymic: <input type="text" name="patronymic" value="" required /></label><br>
	<label>Password: <input type="password" name="pass" value="" required /></label><br>
	<input type="hidden" name="action" value="register" />
	<input type="submit" value="Register" /><br><br>
	<a href="/login.php">Have account?</a>
</form>
</body>
</html>

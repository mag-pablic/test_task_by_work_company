<?php
require( 'class/User.php' );

session_start();

$login = '';
$pass = '';

if ( isset( $_SESSION['login'] ) && isset( $_SESSION['pass'] ) ) {
	$login = $_SESSION['login'];
	$pass = $_SESSION['pass'];
} elseif ( isset( $_POST['login'] ) && isset( $_POST['pass'] ) ) {
	$login = $_POST['login'];
	$pass = $_POST['pass'];
}

if ( isset( $_POST['action'] ) ) {
	switch ($_POST['action']) {
		case 'register':
			User::register(
				$_POST['login'],
				$_POST['pass'],
				$_POST['first-name'],
				$_POST['last-name'],
				$_POST['patronymic'],
				$_POST['email']
			);
			break;
		case 'update':
			User::update(
				$login,
				$_POST['first-name'],
				$_POST['last-name'],
				$_POST['patronymic'],
				$_POST['pass']
			);

			if ( $_POST['pass'] !== '' ) {
				$pass = $_SESSION['pass'] = $_POST['pass'];
			}
			break;
	}
}

User::login( $login, $pass );

if ( User::is_authorised() ) {
	$_SESSION['login'] = $login;
	$_SESSION['pass'] = $pass;
} else {
	header( "HTTP/1.1 302 Moved Temporarily" );
	header( "Location: /login.php" );
	exit();
}

?>

<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta
		name="viewport"
		content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0"
	>
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Account</title>
</head>
<body style="text-align: center;">
<?php $user = User::get_current_user(); ?>
<h1>Welcome to your account page, <?= $user->first_name; ?></h1>

<form action="/" method="post" style="display: inline-block; text-align: right;">
	<h2 style="text-align: center">Your personal data</h2>
	<label>First name: <input type="text" name="first-name" value="<?= $user->first_name; ?>" required /></label><br>
	<label>Last name: <input type="text" name="last-name" value="<?= $user->last_name; ?>" required /></label><br>
	<label>Patronymic: <input type="text" name="patronymic" value="<?= $user->patronymic; ?>" required /></label><br>
	<label>New password: <input type="password" name="pass" value="" /></label><br>
	<input type="hidden" name="action" value="update" />
	<input type="submit" value="update" />
</form>
<div class="tool-bar" style="padding-top: 100px;text-align: center">
	<a href="logout.php">
		<button>exit</button>
	</a>
</div>
</body>
</html>

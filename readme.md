T3

PHP

Создать страницу с авторизацией пользователя: логин и пароль и реализовать в ней:
возможность регистрации пользователя (email, логин, пароль, ФИО),
при входе в "личный кабинет" возможность сменить пароль и ФИО.
использовать "чистый" PHP 5.6 и выше (без фреймворков) и MySQL 5.5 и выше, дизайн не важен, верстка тоже простая. Наворотов не нужно, хотим посмотреть просто Ваш код.

SQL

Есть 2 таблицы

таблица пользователей:
users
----------
`id` int(11)
`email` varchar(55)
`login` varchar(55)

и таблица заказов
orders
--------
`id` int(11)
`user_id` int(11)
`price` int(11)

Необходимо :
1. составить запрос, который выведет список email'лов встречающихся более чем у одного пользователя
2. вывести список логинов пользователей, которые не сделали ни одного заказа
3. вывести список логинов пользователей которые сделали более двух заказов

1.

select email
from users first
WHERE (
  SELECT COUNT(*)
  FROM users second
  WHERE second.email = first.email
) > 1
GROUP BY email;

2. 

SELECT login
FROM users
LEFT JOIN orders o on users.id = o.user_id
WHERE price IS NULL;

3.

SELECT login
FROM users as u
WHERE (
  SELECT COUNT(*)
  FROM users as u_inset
  LEFT JOIN orders o on u_inset.id = o.user_id
  WHERE u_inset.id = u.id
) > 2;
